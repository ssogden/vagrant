# vagrant-ansible

A simple Ansible environment on CentOS 7 with Molecule

synced_folder: /code \
ssh_key: /keys

### Create the environment

```bash
vagrant up
```

### Run provisioners in the environment

```bash
vagrant provision
```

### Stop the environment

```bash
vagrant halt
```

### Destroy the environment

```bash
vagrant destroy
```
