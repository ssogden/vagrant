
#!/usr/bin/env bash

## Install Ansible for provisioning

set -e
set -x

yum -y update
yum -y install python3
yum -y install ansible
