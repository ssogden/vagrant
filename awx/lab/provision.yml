---
- name: prerequisite play
  hosts: ansible_controller
  vars:
    vagrant_user: "vagrant"
    vagrant_shared_directory: "/vagrant"
    vagrant_dns_suffix: "ansible.dev.local"
    docker_users:
      - "{{ vagrant_user }}"
    nodejs_version: "10.x"
    pip_install_packages:
      - docker
      - docker-compose
      - molecule[lint]
      - molecule-docker
    awx_repo_dir: "~/awx"
    awx_run_install_playbook: false
    
  pre_tasks:
    - name: ensure /usr/local/bin exists
      file:
        path: /usr/local/bin
        state: directory
        mode: 0755

    - name: add 'dns' entries for other machines in /etc/hosts
      lineinfile:
        path: /etc/hosts
        regexp: "^{{ item.value.ip }}"
        line: "{{ item.value.ip }} {{ item.key }}.{{ vagrant_dns_suffix }} {{ item.key }}"
      loop: "{{ vagrant_machines | dict2items }}"
      when: vagrant_machines is defined and vagrant_machines

    - name: disable firewalld (for simplicity...)
      service:
        name: firewalld
        state: stopped
        enabled: false
      ignore_errors: true

  roles:
    - geerlingguy.repo-epel
    - geerlingguy.git
    - geerlingguy.ansible
    - geerlingguy.docker
    - geerlingguy.pip
    - geerlingguy.nodejs
    - geerlingguy.awx

  tasks:
    - name: create a symlink for code directory
      file:
        src: "{{ vagrant_shared_directory }}/code"
        dest: "/code"
        owner: vagrant
        group: vagrant
        state: link

    - name: install vim
      package:
        name: vim
        state: present

    - name: add vimrc settings for YAML
      copy:
        content: |
          syntax on
          filetype plugin indent on
          autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab autoindent
        dest: "{{ item }}"
      loop:
        - /root/.vimrc
        - /home/vagrant/.vimrc

    - name: insert the vagrant private  ssh key
      get_url:
        url: https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant
        dest: /home/vagrant/.ssh/id_rsa
        owner: vagrant
        group: vagrant
        mode: 0600

    - name: insert the vagrant public ssh key
      get_url:
        url: https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub
        dest: /home/vagrant/.ssh/id_rsa.pub
        owner: vagrant
        group: vagrant
        mode: 0600

    - name: add ansible hosts
      template:
        src: ansible.hosts.j2
        dest: /etc/ansible/hosts
        owner: root
        group: root
        mode: '0644'
      when: vagrant_machines is defined and vagrant_machines
